#include <msp430.h>
#include <stdbool.h>
#include <stdint.h>

//Per teller een constante. Dan hoeft maar op 1 plek de waarde aangepast te worden.
//Afhankelijk van hoe groot het getal is, kan er een kleiner of groter type gebruiken worden (uint8_t bijvoorbeeld)
#define COUNT_1     500
#define COUNT_2     1000
#define COUNT_3     1500

//PORT 1 defines
    //Voeg hier je eigen defines toe.

//PORT 2 defines
#define LED_RED     1 // Pin 2.1 stuurt de rode led aan
#define LED_GREEN   3 // Pin 2.3 stuurt de groene led aan
#define LED_BLUE    5 // Pin 2.5 stuurt de blauwe led aan

//Vlaggen om te markeren of een teller tot 0 is geweest.
volatile bool counter_1_expired = false;
volatile bool counter_2_expired = false;
volatile bool counter_3_expired = false;

//De interrupt die elke miliseconde uitgevoerd wordt
#pragma vector = TIMER0_A1_VECTOR
__interrupt void milisecondCounter(void)
{
    //Tellers om bij te houden hoe veel miliseconden gewacht moet worden voordat de taak uitgevoerd wordt
    static uint16_t counter_1 = COUNT_1;
    static uint16_t counter_2 = COUNT_2;
    static uint16_t counter_3 = COUNT_3;

    //Als een teller hoger is dan 0, dan wordt de teller met 1 verlaagd. Als deze al 0 is, dan niet. Dit zal weinig gebeuren.
    if(counter_1 != 0)
    {
        counter_1--;
    }
    else
    {
        counter_1 = COUNT_1;
        counter_1_expired = true;
    }
    if(counter_2 != 0)
    {
        counter_2--;
    }
    else
    {
        counter_2 = COUNT_2;
        counter_2_expired = true;
    }
    if(counter_3 != 0)
    {
        counter_3--;
    }
    else
    {
        counter_3 = COUNT_3;
        counter_3_expired = true;
    }
    TA0CTL &= ~TAIFG;
}



//Alle uit te voeren functies
void zet_pin_hoog(uint8_t port, uint8_t pinnummer)
{
    if(port == 1)
    {
        P1OUT |= 1<<pinnummer;
    }
    else if(port == 2)
    {
        P2OUT |= 1<<pinnummer;
    }
}

void zet_pin_laag(uint8_t port, uint8_t pinnummer)
{
    if(port == 1)
    {
        P1OUT &= ~(1<<pinnummer);
    }
    else if(port == 2)
    {
        P2OUT &= ~(1<<pinnummer);
    }
}

void toggle_pin(uint8_t port, uint8_t pinnummer)
{
    if(port == 1)
    {
        P1OUT ^= 1<<pinnummer;
    }
    else if(port == 2)
    {
        P2OUT ^= 1<<pinnummer;
    }
}

/**
 * main.c
 */
int main(void)
{
	WDTCTL = WDTPW | WDTHOLD;	// stop watchdog timer
	if (CALBC1_1MHZ == 0xFF)
    {
        while(1);
    }
    DCOCTL = 0; // Select lowest DCOx and MODx settings
    BCSCTL1 = CALBC1_1MHZ; // Set range
    DCOCTL = CALDCO_1MHZ; // Set DCO step + modulation

    //Zet pinnen 2.1, 2.3 en 2.5 als output
    P2DIR |= 1<<LED_RED | 1<<LED_GREEN | 1<<LED_BLUE;
    //Zet de LEDS uit
    zet_pin_laag(2, LED_RED);
    zet_pin_laag(2, LED_GREEN);
    zet_pin_laag(2, LED_BLUE);

    TA0CTL = TASSEL_2 | ID_0 | MC_1 | TAIE;
    // TASSEL_2 = Timer A clock source select: 1 - SMCLK
    // ID_3 = Timer A input divider: 0 - /1
    // MC_1 = Timer A mode control: 1 - Up to CCR0
    // TAIE = Timer A interrupt enabled
    TA0CCR0 = 999;
    // 1MHz clock, 1000 ticks (0...999) -> 1kHz/1ms interrupt


    __enable_interrupt();
    //Enable het interrupt systeem

    while(1)
    {
        //Als de counter_1_expired true is, dan zorgt de if-statement er voor dat de functie uitgevoerd wordt.
        if(counter_1_expired)
        {
            toggle_pin(2, LED_RED);
            counter_1_expired = false;
        }

        if(counter_2_expired)
        {
            toggle_pin(2, LED_GREEN);
            counter_2_expired = false;
        }

        if(counter_3_expired)
        {
            toggle_pin(2, LED_BLUE);
            counter_3_expired = false;
        }
    }
	return 0;
}
